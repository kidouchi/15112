# hw1.py
# Kelly Idouchi + kidouchi + Section 1

##############################################
## nthFibonacciNumber
##############################################

def nthFibonacciNumber(n):
    return round((( 1/5**0.5 )*( (1 + 5**0.5)/2)**n))
  

##############################################
## isPerfectIntCube
##############################################

def isPerfectIntCube(x):
    return (type(x) == int) and  ((int(round(abs(x)**(1.0/3)))**3 == abs(x)))

##############################################
## getInRange
##############################################

def getInRange(x, bound1, bound2):
   y = min(x, bound1, bound2)
   z = max(x, bound1, bound2)
   return (x+bound1+bound2) - (y+z)

##############################################
## encodeRgba
##############################################

def encodeRgba(red, green, blue, alpha):
    r = getInRange(0,255, red) << 24  
    g = getInRange(0,255, green) << 16
    b = getInRange(0,255, blue)  << 8
    a = getInRange(0,255, alpha) << 0
    return r + g + b + a

##############################################
## isGreen
##############################################

def isGreen(rgba):
  r = (rgba >> 24) 
  g = ((rgba >> 16) & (0x00ff))
  b = ((rgba >> 8) & (0x0000ff))
  a = ((rgba >> 0) & (0x000000ff))
  
  return (g > 0) and (r == 0) and (b == 0) and (a > 0)

##############################################
## getColorFamily
##############################################

def getColorFamily(rgba):
    r = (rgba >> 24) 
    g = ((rgba >> 16) & (0x00ff))
    b = ((rgba >> 8) & (0x0000ff))
    a = ((rgba >> 0) & (0x000000ff))
    g1 = (r>b) and (r>g) and 1
    g2 = (g>b) and (g>r) and 2
    g3 = (b>g) and (b>r) and 3
    g4 = ((r==g) or (r==b) or (g==b) ) and 4
    
    return g1 or g2 or g3 or g4 

##############################################
# getSpecialSection
##############################################

def getSpecialSection(totalStudents, weekNumber):
    t = totalStudents
    w = weekNumber
    max = (abs(-t/30))
    y = 1 + ((w-1)/2)%max
    return chr(64 + y)

##############################################
## bonusGetLetterGrade
##############################################

def bonusGetLetterGrade(score):
    return 42

##############################################
## bonusFindIntRootsOfCubic
##############################################

def bonusFindIntRootsOfCubic(a, b, c, d):
    return 42, 42, 42

######################################################################
##### ignore_rest: The autograder will ignore all code below here ####
######################################################################

def main():
    # include following line to autograde when you run this file
    execfile("hw1-public-grader.py", globals())

if __name__ == "__main__":
    main()
