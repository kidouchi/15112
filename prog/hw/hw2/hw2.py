# hw2.py
# Kelly Idouchi + kidouchi + B

########################################

## nthWithProperty309

## 309 is the smallest number whose 5th power contains every digit at least once.

########################################

 

 # We will say that a number n has "Property309" if its 5th power contains

 # every digit at least once.  309 is the smallest number with this property.

 # Write the function nthWithProperty309 that takes a non-negative

 # int n and returns the nth number with Property309.

 
def lookForNum(x,n):
    y = 0
    check = x
    count = 0
    while(n > 0):
        y = n % 10
        if(y == check):
            count += 1    
        n = n/ 10
    return count 

def isProperty309(n):
    i = 0
    y = 0
    count = 0
    number = n**5

    if(n < 0 or type(n) == type(1.0)):
        return 0
    if(n == 0):
        return 309
    while(n > 0 and i < 10):
        if(lookForNum(i, number) > 0):
            count += 1
        i += 1    
            
    if(count == 10):
        return True
    else:
        return False
    
        

def nthWithProperty309(n):
    i = 309
    count = 0

    if(n < 0 or type(n) != type(1)):
        return 0
    if(n == 0):
        return 309
    while(n > 0 and count < n):
        i += 1
        if(isProperty309(i)):
            count += 1
    return i


########################################

## nthSquareSumOfTwoSquares

########################################

 

# Write the function nthSquareSumOfTwoSquares that takes a non-negative

# int n and returns the nth number that is itself a perfect square and is

# also the sum of two other positive perfect squares.

# For example, the first such number is 25, because 25=5**2, and

# 25 = 16 + 9 = 4**2 + 3**2

 
def isSquare(n):
    c = n
    a = 1
    x = 0
    y = 0
    epsilon = 0.000001
    while(a < n):
        sub = c**2 - a**2
        if(abs(sub**(1.0/2) - int(sub**(1.0/2))) < epsilon):
            x = sub
            y = (c**2 - sub)
        a += 1

    if((x + y) == c**2):
        return True
    else:
        return False

def nthSquareSumOfTwoSquares(n):
    i = 5
    count = 0

    if(n < 0 or type(n) != type(1)):
        return 0
    if(n == 0):
        return 25
    while(n > 0 and count < n):
        i += 1
        if(isSquare(i)):
            count += 1
    return i**2

########################################

## nthSmithNumber

########################################

 

# Write the function nthSmithNumber that takes a non-negative

# int n and returns the nth Smith number, where a Smith number

# is a composite (non-prime) the sum of whose digits are the sum

# of the digits of its prime factors (excluding 1).  Note that if

# a prime number divides the Smith number multiple times, its digit

# sum is counted that many times.  For example, 4 equals 2**2, so

# the prime factor 2 is counted twice, thus making 4 a Smith Number.

 
def isPrime(n):
    if(n < 2):
        return False
    if(n == 2):
        return True
    if(n % 2 == 0):
        return False
    maxFactor = int(round(n**0.5))
    for factor in xrange(2, maxFactor+1):
        if(n % factor == 0):
            return False
    return True

#Week 2, Section : Loops, Letterh
#By using isPrime, it makes sure that the numbers are prime. Guess
#will be returned as the nth Prime number. Found is a counting device 
#that adds up each time a prime number is found. Found will be the nth of the list.
def nthPrime(n):
    found = 0
    guess = 0
    while(found <= n):
        guess += 1
        if(isPrime(guess)):
            found += 1
    return guess


def sumDigNum(n):
    y = 0
    sum = 0
    if(n < 0 or type(n) == type(1.0)):
        return 0
    while(n > 0):
        y = n % 10
        sum += y
        n = n / 10
    return sum

def ifPrimeThenDigSum(n):
    sum = 0
    
    sum = sum + sumDigNum(n)

    return sum


def primeSum(n):
    i = 0
    sum = 0
    
    while(i < n):
        if(isPrime(n) and (i == 0)):
            sum = sum + n
            break
        while((n % nthPrime(i) == 0) and (nthPrime(i) < 10) and (n > 1)):
            sum = sum + nthPrime(i)
            n = n / nthPrime(i)
        if((n % nthPrime(i) == 0) and (nthPrime(i) >= 10)):
            sum = sum + ifPrimeThenDigSum(nthPrime(i))
            n = n / nthPrime(i)
        if((isPrime(n)) and (n > 10) and (i > 0)):
            sum = sum + ifPrimeThenDigSum(n) 
            break
        i += 1
    return sum


def nthSmithNumber(n):
    i = 4
    count = 0
    if(n < 0 or type(n) != type(1)):
        return 0
    if(n == 0):
        return 4
    while(count < n):
        i += 1
        if((sumDigNum(i) == primeSum(i)) and i > 9):
            count += 1
    return i


 
########################################

## nthKaprekarNumber

########################################

 

# Adapted from http://en.wikipedia.org/wiki/Kaprekar_number

# In mathematics, a Kaprekar number is a non-negative integer,

# the representation of whose square can be split into two parts

# (where the right part is not zero) that add up to the original number again.

# For instance, 45 is a Kaprekar number, because 45**2 = 2025 and 20+25 = 45.


def kaprekarSum(n):
    y = 1
    x = 1
    sum = 0
    mod = 1
    squareroot = int(n**0.5)


    while(x > 0):
        
        y = n % (10**mod)
        x = (n - y)/(10**mod)
        sum = x + y
        
        if((sum == squareroot) and (not((x == 0) or (y == 0)))):
            return sum
            break
        else:
            mod += 1
    return sum

def nthKaprekarNumber(n):
    i = 1
    count = 0
    
    if(n < 0 or type(n) != type(1)):
        return 0
    if(n == 0):
        return 1
    while(n > 0 and count < n):
        i += 1
        if(i == (kaprekarSum(i**2))):
            count += 1
    return i        


#####################################################################
##### ignore_rest: The autograder will ignore all code below here ####
######################################################################


def main():
    # include following line to autograde when you run this file
    execfile("hw2-public-grader.py", globals())

if __name__ == "__main__":
    main()



def testing():

    assert(nthWithProperty309(-1) == 0)
    assert(nthWithProperty309(3.565656) == 0)
    assert(nthWithProperty309(-1.0) == 0)
    assert(nthWithProperty309(-3.556456) == 0)
    assert(nthWithProperty309("This sucks!") == 0)
    assert(nthWithProperty309(0) == 309)
    assert(nthWithProperty309(1) == 418)
    assert(nthWithProperty309(2) == 462)
    assert(nthWithProperty309(3) == 474)
    print"Passed 1!"


    assert(nthSquareSumOfTwoSquares(-1) == 0)
    assert(nthSquareSumOfTwoSquares(-1.0) == 0)
    assert(nthSquareSumOfTwoSquares(3.565656) == 0)
    assert(nthSquareSumOfTwoSquares(-3.5534534) == 0)
    assert(nthSquareSumOfTwoSquares("This sucks!") == 0)
    assert(nthSquareSumOfTwoSquares(0) == 25)
    assert(nthSquareSumOfTwoSquares(1) == 100)
    assert(nthSquareSumOfTwoSquares(2) == 169)
    assert(nthSquareSumOfTwoSquares(3) == 225)
    assert(nthSquareSumOfTwoSquares(42) == 10201)
    print "Passed 2!"

    assert(nthSmithNumber(-1) == 0)
    assert(nthSmithNumber(1.0) == 0)
    assert(nthSmithNumber(-1.0) == 0)
    assert(nthSmithNumber(3.5252) == 0)
    assert(nthSmithNumber(-3.42423) == 0)
    assert(nthSmithNumber("This sucks!") == 0)
    assert(nthSmithNumber(0) == 4)
    assert(nthSmithNumber(1) == 22)
    assert(nthSmithNumber(2) == 27)
    assert(nthSmithNumber(3) == 58)
    assert(nthSmithNumber(10) == 274)
    assert(nthSmithNumber(6) == 121)
    assert(nthSmithNumber(7) == 166)
    assert(nthSmithNumber(20) == 517)
    assert(nthSmithNumber(39) == 778)
    assert(nthSmithNumber(49) == 1086)
    print "Passed 3!"


    assert(nthKaprekarNumber(-1) == 0)
    assert(nthKaprekarNumber(1.0) == 0)
    assert(nthKaprekarNumber(-1.0) == 0)
    assert(nthKaprekarNumber(3.525252) == 0)
    assert(nthKaprekarNumber(-3.52525) == 0)
    assert(nthKaprekarNumber("This sucks!") == 0)
    assert(nthKaprekarNumber(0) == 1)
    assert(nthKaprekarNumber(1) == 9)
    assert(nthKaprekarNumber(2) == 45)
    assert(nthKaprekarNumber(3) == 55)
    assert(nthKaprekarNumber(4) == 99)
    assert(nthKaprekarNumber(5) == 297)
    assert(nthKaprekarNumber(7) == 999)
    assert(nthKaprekarNumber(10) == 4879)
    assert(nthKaprekarNumber(12) == 5050)
    assert(nthKaprekarNumber(15) == 7777)
    assert(nthKaprekarNumber(16) == 9999)
    assert(nthKaprekarNumber(17) == 17344)
    assert(nthKaprekarNumber(18) == 22222)
    assert(nthKaprekarNumber(23) == 99999)
    assert(nthKaprekarNumber(30) == 329967)
    assert(nthKaprekarNumber(37) == 500500)
    print "Passed 4!"

