# Kelly Idouchi + kidouchi + Section B

#Take string and finds the letter by n shifts and replaces with new letters

import string

#Encodes text by shifting each character of string by n shifts
def encodeText(s,n):
    result = ""
    lower = string.ascii_lowercase
    upper = string.ascii_uppercase
    punct = string.punctuation
    if (len(s) == 0):
        return 0
    #Goes through each character of string
    for i in range(len(s)):  
        if(s[i] == " "):
            result += " "
        for x in range(len(lower)):
            #Shifts forward n places
            if(s[i] == lower[x]):
                #Makes sure if goes past "z" starts back at "a"
                result += lower[(x+n) % 26]
        for y in range(len(upper)):
            if(s[i] == upper[y]):
                result += upper[(y+n) % 26]
        for p in range(len(punct)):
            if(s[i] == punct[p]):
                result += punct[p]
    return result

#Decodes text by shifting characters back n shifts
def decodeText(s,n):
    result = ""
    lower = string.ascii_lowercase
    upper = string.ascii_uppercase
    punct = string.punctuation
    if(len(s) == 0):
        return 0
    #Goes through each character
    for i in range(len(s)):
        if(s[i] == " "):
            result += " "
        for x in range(len(lower)):
            if(s[i] == lower[x]):
                #Shifts backwards n places
                result += lower[(26 +(x-n))%26]
        for y in range(len(upper)):
            if(s[i] == upper[y]):
                result += upper[(26+(y-n))%26] 
        for p in range(len(punct)):
            if(s[i] == punct[p]):
                result += punct[p]
    return result

def observed(s, clower, cupper):
    count = 0
    total = len(s)
    if(len(s) == 0):
        return 0
    #Goes through each character of string    
    for i in range(len(s)):
        if(s[i] == clower):
            count += 1
        elif(s[i] == cupper):
            count += 1 
    return float(count)/total

#Takes a string's chi squared value
def chiSquared(s):
    if(len(s) == 0):
        return 0
    chiE = (((observed(s, "e", "E") - 0.12702)**2) / 0.12702) 
    chiL = (((observed(s, "l", "L") - 0.04025)**2) / 0.04025)
    chiK = (((observed(s, "k", "K") - 0.00772)**2) / 0.00772)
    chi = chiE + chiL + chiK
    return chi

#Find shift with lowest chi squared value
def crackCode(s):  
    bestShift = 0
    if(len(s) == 0):
        return 0
    smallestSoFar = chiSquared(s)
    #Goes through all possible shifts
    for shift in range(26):
        chiNew = chiSquared(encodeText(s, shift))
        #Compares and finds smallest chi squared value
        if(chiNew < smallestSoFar):
            smallestSoFar = chiNew
            bestShift = (26-shift)%26
    return bestShift

def deleteComments(code):
    start = code.find("#")
    end = len(code) + 1
    code = code - (code[start:] + code[:end])
    return code

def deleteQuotes(code):
    start = code.find('\'')
    for c in code:
        




def newCode(code):
    code = code.replace('\"', " ")
    code = code.replace('\'', " ")
    code = deleteComments(code)




#Checks to see if there is balanced amt of ()
def hasBalancedParentheses(code):
    code = newCode(code):
    parenLeft = 0 
    parenRight = 0
    for c in code:
        if(c == '('):
            parenLeft += 1
        elif(c == ')'):
            parenRight += 1
    if(parenLeft == parenRight):
        return True
    else:
        return False








######################################################################
##### ignore_rest: The autograder will ignore all code below here ####
######################################################################

#From Data and Expressions lesson
#Makes sure number doesn't have leading decimals
def almostEqual(d1, d2):
    epsilon = 0.000001
    return (abs(d2 - d1) < epsilon)

def testEncodeText():
    assert(encodeText("We attack at dawn!", 5) == "Bj fyyfhp fy ifbs!")
    assert(encodeText("Hello!", 3) == "Khoor!")
    assert(encodeText("""Hello!
                         Hello!
                         Hello!
                         """, 3) == "Khoor! Khoor! Khoor!")
    assert(encodeText("", 1) == 0)
    print "Passed!"


def testDecodeText():
    assert(decodeText("Bj fyyfhp fy ifbs!", 5) == "We attack at dawn!")
    assert(decodeText("Khoor!", 3) == "Hello!")
    assert(decodeText("", 1) == 0)
    print "Passed!"

def testObserved():
    assert(almostEqual(observed("Hello", "e", "E"), 0.2))
    assert(almostEqual(observed("Hello!?", "e", "E"), 0.2))
    assert(almostEqual(observed("", "l", "L"), 0.0))
    assert(almostEqual(observed("""Heck
                                   Heck
                                   Heck
                                   """, "k", "K"), 0.25))
    print "Passed!"


def testChiSquared():
    assert(almostEqual(chiSquared("eat kind lit!?"), 1.197501))
    assert(almostEqual(chiSquared("hello"), 3.265056))
    assert(almostEqual(chiSquared(""), 0.0))
    print "Passed!"

def testCrackCode():
    print "testing crackCode()...",
    plaintext = (
"""
CHAPTER I. Down the Rabbit-Hole
Alice was beginning to get very tired of sitting by her sister on the bank,
and of having nothing to do: once or twice she had peeped into the book her
sister was reading, but it had no pictures or conversations in it, 'and what
is the use of a book,' thought Alice 'without pictures or conversation?'
So she was considering in her own mind (as well as she could, for the hot day
made her feel very sleepy and stupid), whether the pleasure of making a
daisy-chain would be worth the trouble of getting up and picking the daisies,
when suddenly a White Rabbit with pink eyes ran close by her.
""")
    for shift in xrange(26):
        ciphertext = encodeText(plaintext, shift)
        crackedShift = crackCode(ciphertext)
        assert(shift == crackedShift)
    print "passed!"






