# hw4.py
# Kelly Idouchi + kidouchi + Section B

#####################################################################
#####CONTRACT########################################################
#####################################################################
import functools
def contract(f):

    def callIfExists(fn, suffix, *args, **kwargs):
        fnName = fn.__name__ + suffix
        if (fnName in globals()): return globals()[fnName](*args, **kwargs)
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        if (__debug__): reqResult = callIfExists(f, "_requires", *args, **kwargs)
        result = f(*args, **kwargs) 
        if (__debug__):
            if (reqResult == None):
                callIfExists(f, "_ensures", result, *args, **kwargs)
            else:
                callIfExists(f, "_ensures", reqResult, result, *args, **kwargs)
        return result
    return wrapper
#####################################################################
#as taken from Week#1 Lesson
def almostEqual(d1, d2):
    epsilon = 0.001
    return (abs(d2 - d1) < epsilon)

def almostEqualTuple(d1, d2): 
    epsilon = 0.01
    checkList = [ ]
    for i in xrange(len(d1)):
        checkList.append((abs(d2[i] - d1[i]) < epsilon))
    for item in checkList:
        if(item == False):
            return False
    return True
#######################################################################
import copy

# USE IN CONTRACT
# makes sure list contains ONLY STRINGS
def isStringList(listOf):
    for item in listOf:
        if(not isinstance(item,str)):
            return False
    return True

# USE IN CONTRACT
# makes sure that it is a LIST
def isList(listOf):
    if(isinstance(listOf,list)):
        return True
    else:   
        return False

#precondition
def mostCommonName_requires(listOfNames):
    assert(isList(listOfNames) and isStringList(listOfNames))

#postcondition
def mostCommonName_ensures(result,listOfNames):
    assert((isList(result) and isStringList(result)) or isinstance(result,str))

def eachName(listOfNames):
    nameList = [ ]
    for name in listOfNames:
        if name not in nameList:
            nameList.append(name)
    return nameList

@contract
# finds name(s) with most occurrences in list
def mostCommonName(listOfNames):
    mostCounts = 0 
    commonName = [ ]
    nameList = eachName(listOfNames) #instance of each name
    if(listOfNames == [ ]):
        return None
    for name in nameList:
        count = 0
        for names in listOfNames:
            if(name == names):
                count = listOfNames.count(name)
        if(count > mostCounts):
            mostCounts = count
            commonName = [name]
        elif(count == mostCounts):
            commonName.append(name)
    commonName = sorted(commonName)
    if(len(commonName) == 1):
        return commonName[0]
    else:
        return commonName

###########################################################################
# From Lesson Week #5
# makes sure value is a REAL NUMBER
def isReal(x):
    # From Python 2.6 onwards, can just do: isinstance(x, numbers.Real)
    # But we'll make this compatible with Python 2.5 (which runs 
    #on linux.andrew)
    return(isinstance(x,float)or isinstance(x,int)or isinstance(x,long))

# USE IN CONTRACT
# makes sure the list contains ONLY NUMBERS
def isNumberList(listOfPairs):
    numList = [ ]
    xList, yList = zip(*listOfPairs)
    #creates list only containing numbers
    for item in xList:
    #makes sure each item in list is either an int, float, or long    
        if(not isReal(item)):
            return False
    for item in yList:
        if(not isReal(item)):
            return False
    return True

#precondition
def areaOfPolygon_requires(listOfPairs):
    assert((isList(listOfPairs)) and (isNumberList(listOfPairs)))
    assert(isTuple(listOfPairs))

#postcondition
def areaOfPolygon_ensures(result,listOfPairs):
    assert((isReal(result)) and (result >= 0))

@contract
# Finds area of a Polygon
def areaOfPolygon(listOfPairs):
    numList = [ ]
    num1 = 0
    num2 = 0
    length = len(listOfPairs)
    if(listOfPairs == [ ]):
        return None
    for item in listOfPairs:
        b = list(eval(str(item)))
        numList += b
    numList = numList + list(eval(str(listOfPairs[0]))) 
    for i in xrange(0,len(numList) - 3,2):
        num1 += numList[i] * numList[i+3]
    for i in xrange(0,len(numList) - 2,2):
        num2 += numList[i+1] * numList[i+2]
    sum = num1 - num2
    area = abs(sum)/float(2)
    return area
############################################################
def isTuple(listOfPairs):
    for item in listOfPairs:
        if(len(item) > 2):
            return False
    return True

def isThreeValues(values):
    if(len(values) > 3):
        return False
    return True

# precondition
def linearRegression_requires(orderedPairs):
    assert(isList(orderedPairs) and isNumberList(orderedPairs))
    assert(isTuple(orderedPairs))

# postcondition
def linearRegression_ensures(result, orderedPairs):
    assert(isReal(result[0]))
    assert(isReal(result[1]))
    assert(isReal(result[2]))
    assert(isThreeValues(result))
# find average of list of numbers
def averageOf(listOf):
    sum = 0
    count = 0
    for i in range(len(listOf)):
        sum += listOf[i]
        count += 1
    return float(sum)/count

# sum of (xi - x) **2 o r (yi - y)**2
# Find the standard deviation of the list
def standardDeviation(listOf):
    sum = 0
    for i in range(len(listOf)):
        sum += ((listOf[i] - averageOf(listOf))**2) 
    return sum

# sum of (xi-x)(yi-y)
# finds the standard deviation of list x and y multiplied
def stanDevOfXY(listx, listy):
    sum = 0
    x = 0
    y = 0
    for i in range(len(listx)):
        x = listx[i] - averageOf(listx)
        y = listy[i] - averageOf(listy)
        sum += (x*y)
    return sum

# sum of (yi - yihat)**2
# finds the residual of the line
def residual(listx, listy, slope, y_intercept):
    sum = 0
    for i in range(len(listx)):
        #plugs each x-value into linear regression formula
        yHat = (slope * listx[i]) + y_intercept
        sum += ((listy[i] - yHat) ** 2)
    return sum

# finds the r coefficient
def rcoefficient(res, dev):
    rSquared = abs((dev - res) / dev)
    return rSquared **(1.0/2)

@contract
# finds the slope, y-intercept, and r coefficient from the given ordered pairs
def linearRegression(orderedPairs):   
    x, y = zip(*orderedPairs)
    if(orderedPairs == [ ]):
        return None
    xavg = averageOf(x)
    yavg = averageOf(y)
    SSxx = standardDeviation(x)
    SSxy = stanDevOfXY(x,y)
    slope = SSxy / SSxx
    y_intercept = yavg - (slope * xavg)
    SSres = residual(x,y, slope, y_intercept)
    SSdev = standardDeviation(y)
    coefficient = rcoefficient(SSres, SSdev)
    lineReg = slope,y_intercept,coefficient
    return lineReg
##################################################################
#makes sure length is N**2
def isSquared(num):
    if((num == 0) or (num < 0)):
        return False
    num = num**(1.0/2)
    if(almostEqual((int(num) - num), 0.0)):      
        return True
    else: 
        return False

#makes sure contains only integers in range 0 to N**2
#takes a list
def isInRange(values):
    maxRange = 0
    if(isSquared(len(values))):
        maxRange = len(values)
    for item in values:
        if((item < 0) or (item > maxRange)):
            return False
    return True

#makes sure list only contains POSITIVE INTEGERS
def isPosIntegerList(values):
    for item in values: 
        if((not isinstance(item, int)) and (item < 0)):
            return False
        return True

def areLegalValues_requires(values):
    assert(isPosIntegerList(values) and isInRange(values))

def areLegalValues_ensures(result, values):
    assert(isinstance(result, bool))

@contract
#takes a list and sees if values are legal in sudoku
#values is positive integer list within range of 0 and N**2
def areLegalValues(values):   
   if(isPosIntegerList(values)):
        for item in values:    
            if(values.count(item) > 1 and (item != 0)):
                return False
        return True

#checks if board is N**2 by N**2
#USE IN CONTRACT
def isBoard(board):
    row = len(board)
    col = len(board[0])
    if(row == col):
        if(isSquared(row) and isSquared(col)):
            return True
    return False   

#makes sure row/col/block number is in range 0 to (N**2-1)
#assume board is N**2 by N**2
#USE IN CONTRACT
def isInRangeRCB(board, rcb):
    if(type(rcb) == type(2)):
        if((rcb >= 0) and (rcb <= (len(board) - 1))):
            return True
    return False

def isLegalRow_requires(board,row):
    assert(isBoard(board) and isInRangeRCB(board,row))
    assert(isInRange(board[row]))

def isLegalRow_ensures(result,board,row):
    assert(isinstance(result,bool))

@contract
#checks to see if Row is legal in Sudoku
def isLegalRow(board,row):
    rowList = board[row]
    if(areLegalValues(rowList)):
        return True
    else:
        return False

# makes sure col number is between 0 and N**2
def isInRangeCol(board,col):
    rows = len(board)
    for row in xrange(rows):
        if((board[row][col] < 0) or (board[row][col] > rows)):
            return False
    return True
    
def isLegalCol_requires(board,col):
    assert(isBoard(board) and isInRangeRCB(board,col))
    assert(isInRangeCol(board,col))
    
def isLegalCol_ensures(result, board, col):
    assert(isinstance(result,bool))

@contract
#checks to see if Column is legal in Sudoku
def isLegalCol(board,col):
    row = len(board) 
    colList = [ ]
    for index in xrange(row):
        colList.append(board[index][col])
    if(areLegalValues(colList)):
        return True
    else:   
        return False

def isLegalBlock_requires(board,block):
    assert(isBoard(board) and isInRangeRCB(board,block)) 

def isLegalBlock_ensures(result,board,block):
    assert(isinstance(result,bool))

@contract
#makes sure list in block is legal in Sudoku
def isLegalBlock(board,block):
    blockList = [ ]
    row = len(board)
    col = len(board[0])
    n = int(row**(1.0/2))
    startRow =  ((n*block)/row)*n
    startCol =  (block*n)%n
    endRow = startRow + n
    endCol = startCol + n   
    for rows in xrange(startRow, endRow):
        for cols in xrange(startCol, endCol):
            blockList.append(board[rows][cols])
            print "blist is =", blockList
    if(areLegalValues(blockList)):
        return True
    else:
        return False

def isLegalSudoku_requires(board):
    assert(isBoard(board))

def isLegalSudoku_ensures(result,board):
    assert(isinstance(result, bool))

@contract
#FINALLY! Makes sure the whole board is legal for sudoku!!!!!
def isLegalSudoku(board):
    nByNBoard = len(board) 
    for index in xrange(nByNBoard):
        if(not isLegalRow(board, index)):
            return False
        elif(not isLegalCol(board, index)):
            return False
        elif(not isLegalBlock(board, index)):
            return False
    return True


######################################################################
##### ignore_rest: The autograder will ignore all code below here ####
######################################################################

def testingMain():
    
    assert(mostCommonName([ ]) == None)
    assert(mostCommonName(["Jane", "Aaron", "Jane", "Cindy", "Aaron"]) == ["Aaron", "Jane"])
    assert(mostCommonName(["Jane", "Jane"]) == "Jane")
    assert(mostCommonName(["Adam"]) == "Adam")
    assert(mostCommonName(["Adam", "Eric", "Ben", "Ben", "Adam", "Jane", "Adam"]) == "Adam")
    print "Passed mostCommonName!"

    assert(almostEqual(areaOfPolygon([(4,10), (9,7), (11,2), (2,2)]), 45.5))
    print "Passed areaOfPolygon!"

    assert(almostEqualTuple(linearRegression([(1,3), (2,5), (4,8)]) , (1.6429, 1.5, 0.9972)))
    print "Passed linearRegression!"


    assert(areLegalValues([0,0,0,0]) == True)
    assert(areLegalValues([0,1,2,3]) == True)
    assert(areLegalValues([1,2,3,4]) == True)
    assert(areLegalValues([0,0,0,0,0,0,0,0,0]) == True)
    assert(areLegalValues([0,1,2,3,4,5,6,7,8]) == True)
    assert(areLegalValues([1,2,3,4,5,6,7,8,9]) == True)
    assert(areLegalValues([1,2,3,5]) == False)
    assert(areLegalValues([1,1,2,3]) == False)
    assert(areLegalValues([1,2,3,10]) == False)
    assert(areLegalValues([1,1,2,3,4,5,6,7,8]) == False)
    assert(areLegalValues([0,0,1,2,2,3,4,5,7]) == False)
    assert(areLegalValues([0,1,2,3,10,7,8,9,0]) == False)
    print "Passed areLegalValues!"

    
    assert(isLegalRow(([ [0,1,2,3],
                        [1,2,3,4],
                        [2,0,0,0],
                        [3,0,0,0]
                      ]), 1) == True)
    assert(isLegalRow(([ [0,1,2,3],
                        [1,2,3,4],
                        [2,2,0,0],
                        [3,0,0,0]
                      ]), 2) == False)
    assert(isLegalRow(([ [0,1,2,3],
                        [0,2,3,4],
                        [0,0,0,0],
                        [0,0,0,0]
                      ]), 2) == True)
    print "Passed isLegalRow!"


    assert(isLegalCol(([ [0,1,2,3],
                        [1,2,3,4],
                        [2,0,0,0],
                        [3,0,0,0]
                      ]), 3) == True)
    assert(isLegalCol(([ [0,0,2,3],
                        [0,0,3,4],
                        [0,0,0,0],
                        [0,0,0,0]
                      ]), 0) == True)
    assert(isLegalCol(([ [0,1,2,3],
                        [1,2,3,4],
                        [2,2,0,0],
                        [3,0,0,0]
                      ]), 1) == False)
    print "Passed isLegalCol!"

    assert(isLegalBlock(([ [0,1,2,3],
                          [1,2,3,4],
                          [2,2,0,0],
                          [3,0,0,0]
                        ]), 0) == False) 
    assert(isLegalBlock(([ [0,1,1,2],
                          [1,2,3,4],
                          [2,2,0,0],
                          [3,0,0,0]
                        ]), 1) == True)
    assert(isLegalBlock(([ [0,1,2,3],
                          [1,2,3,4],
                          [2,2,0,0],
                          [3,0,0,0]
                        ]), 2) == False)
    assert(isLegalBlock(([ [0,1,2,3],
                          [1,2,3,4],
                          [2,2,0,0],
                          [3,0,0,0]
                        ]), 3) == False)
    print "Passed isLegalBlock!"

def testingHelper():

    assert(isStringList(["Adam", "Bob"]) == True)
    assert(isStringList([1,2,3]) == False)
    assert(isStringList(["Hey"]) == True)
    assert(isStringList([ ]) == False)
    assert(isStringList(["Hola", 1, True, 1.0]) == False)
    print "Passed isStringList!"

    assert(isList([ ]) == True)
    assert(isList([1, 2, 3]) == True)
    assert(isList(["Cola", "Sprite", "Pepsi"]) == True)
    assert(isList([1.0, 2.3344, 0.0]) == True)
    assert(isList([1, 1.0, True, "Hey"]) == True)
    print "Passed isList!"

    assert(isNumberList([(1, 2), (3, 4)]) == True)  
    assert(isNumberList([(0,0),(0,0)]) == True)
    assert(isNumberList([("Hey", "Math"), ("Numbers", "Awesome")]) == False)
    assert(isNumberList([(1.0, 5.3333), (3.5,5.6)]) == True) 
    assert(isNumberList([(-1.0, -1), (1, 1.0)]) == True) 
    print "Passed isNumberList!"

    assert(almostEqual(averageOf([1, 2, 4]), 2.333))
    assert(almostEqual(averageOf([3, 5, 8]), 5.333))
    print "Passed averageOf!"
   
    assert(almostEqual(standardDeviation([1, 2, 4]), 4.666))
    assert(almostEqual(standardDeviation([3, 5, 8]), 12.666))
    print "Passed standardDeviation!"
    
    assert(almostEqual(stanDevOfXY([1,2,4],[3,5,8]) , 7.666))
    print "Passed stanDevOfXY!"

    assert(almostEqual(residual([1,2,4],[3,5,8], 1.64, 1.51) , 0.071))
    print "Passed almostEqual!"

    assert(almostEqual(rcoefficient(0.071, 12.67), 0.997))
    print "Passed rcoefficient!"

    assert(isSquared(25) == True)
    assert(isSquared(26) == False)
    assert(isSquared(0) == False)    
    assert(isSquared(-1) == False)
    assert(isSquared(4) == True)
    assert(isSquared("Hey") == False)
    assert(isSquared(True) == False)
    # only want integers 
    assert(isSquared(1.0) == False)
    print "Passed isSquared!"

    assert(isInRange([0,0,0,0]) == True)
    assert(isInRange([1,2,3,4]) == True)
    assert(isInRange([0,1,2,3]) == True)
    assert(isInRange([0,1,2,5]) == False)
    assert(isInRange([0,1,2]) == False)
    assert(isInRange([0]) == True)
    print "Passed isInRange!"

    assert(isPosIntegerList([1,2,3,5]) == True)
    assert(isPosIntegerList([2, -1, 5]) == False)
    assert(isPosIntegerList([3.0, 3, 5]) == False)
    # in this case we want 0 for Sudoku
    assert(isPosIntegerList([0]) == True)
    print "Passed isPosIntegerList!"

    assert(isBoard([   [0,2,3,4,5,6,7,8,9],
                       [1,2,3,4,5,6,7,8,9],
                       [2,2,3,4,5,6,7,8,9],
                       [3,2,3,4,5,6,7,8,9],
                       [4,0,0,0,0,0,0,0,0],
                       [5,0,0,0,0,0,0,0,0],
                       [6,0,0,0,0,0,0,0,0],
                       [7,0,0,0,0,0,0,0,0]
                   ]) == False)
    assert(isBoard([   [0,2,3,4,5,6,7,8,9],
                       [1,2,3,4,5,6,7,8,9],
                       [2,2,3,4,5,6,7,8,9],
                       [3,2,3,4,5,6,7,8,9],
                       [4,0,0,0,0,0,0,0,0],
                       [5,0,0,0,0,0,0,0,0],
                       [6,0,0,0,0,0,0,0,0],
                       [7,0,0,0,0,0,0,0,0],
                       [8,0,0,0,0,0,0,0,0]
                   ]) == True)
    assert(isBoard([ [0,1,2,3],
                     [1,2,3,4],
                     [2,0,0,0],
                     [3,0,0,0]
                   ]) == True)
    assert(isBoard([[0,1,2,3]])  ==  False)       
    assert(isBoard([[]]) == False)            
    print "Passed isBoard!"

    assert(isInRangeRCB([ [0,1,2,3],
                          [1,2,3,4],
                          [2,0,0,0],
                          [3,0,0,0]
                        ] , 2) == True)
    assert(isInRangeRCB([ [0,1,2,3],
                          [1,2,3,4],
                          [2,0,0,0],
                          [3,0,0,0]
                        ] , 4) == False)
    assert(isInRangeRCB([ [0,1,2,3],
                          [1,2,3,4],
                          [2,0,0,0],
                          [3,0,0,0]
                        ] , 0) == True)
    assert(isInRangeRCB([ [0,1,2,3],
                          [1,2,3,4],
                          [2,0,0,0],
                          [3,0,0,0]
                        ] , 5) == False)
    assert(isInRangeRCB([ [0,1,2,3],
                          [1,2,3,4],
                          [2,0,0,0],
                          [3,0,0,0]
                        ] , 1.0) == False)
    assert(isInRangeRCB([ [0,1,2,3],
                          [1,2,3,4],
                          [2,0,0,0],
                          [3,0,0,0]
                        ] , 3.5555) == False)
    print "Passed isInRangeRCB!"

    assert(isInRangeCol([  [0,1,2,3],
                           [0,2,0,0],
                           [0,3,3,4],
                           [0,4,2,2]
                         ], 1) == True)
    assert(isInRangeCol([  [0,1,2,3],
                           [0,2,0,0],
                           [0,3,3,4],
                           [0,5,2,2]
                         ], 1) == False)
    assert(isInRangeCol([  [0,1,2,3],
                           [0,2,0,0],
                           [0,3,3,4],
                           [0,4,2,2]
                         ], 0) == True)

    print "Passed isInRangeCol!"    
    






