# hw6.py
# kidouchi
# Section B

class Foo(object):
    countX = -1
    countY = -1

    def __init__(self,x,y=1):
        self.countCalls = 0
        self.x = x 
        self.y = y

    @classmethod
    def nextPair(cls):
        Foo.countX += 1
        Foo.countY += 1
        return Foo(Foo.countX, Foo.countY)
    
    def callsToGetSum(self):
        return self.countCalls

    def getSum(self):
        self.countCalls += 1
        return self.x + self.y

    def addValues(self,func):
        self.x = self.x + func.x
        self.y = self.y + func.y
        return self.x,self.y
    
    def __add__(self,other):
        newX = self.x + other.x
        newY = self.y + other.y
        return Foo(newX, newY)    

    def __repr__(self):
        return "Foo(%r,%r)" % (self.x ,self.y)

    def __eq__(self,other):
        if not ((type("s") == type(self)) or (type("s") == type(other))):
            return (self.x == other.x) and (self.y == other.y)
        else:   
            return False
        
    def __str__(self,other):
        return "Foo(%s, %s)" % (self.x, self.y)

    def __hash__(self):
        hashables = (self.x, self.y)
        result = 0
        for value in hashables:
            result = 33*result + hash(value)
        return hash(result)

class Person(object):

    def __init__(self,name):
        self.name = name
        self.parent = None
        self.children = [ ]
    
    def addChild(self,name):
        child = Person(name)
        child.parent = self
        self.children += [child]
        return child

    def getChildren(self):
        return self.children

    def __repr__(self):
        return "Person(%s)"  % (self.name)

    def getSiblings(self):
        siblings = [ ]
        if self.parent == None:
            return siblings
        else:   
            parent = self.parent
            for sibs in parent.getChildren():
                if sibs != self:
                    siblings += [sibs]
            return siblings
    
    def getCousins(self):
        cousins = [ ] 
        if self.parent == None:
            return cousins
        else:
            parent = self.parent
            for parcuz in parent.getSiblings():
                cousins += parcuz.getChildren()
            return cousins

    def getOffspring(self):
        offspring = [ ]
        if self.getChildren() == []:
            return [ ] 
        else:
            for child in self.getChildren():
                offspring += [child]
                offspring += child.getOffspring()
            return offspring


class Vehicle(object):
    def __init__(self,x=1):
        self.x = x
    
    def __repr__(self):
        return "Vehicle()"
        
    def getWheels(self):
        raise NotImplementedError


class Car(Vehicle):
    def __init__(self, license, mpg):
        self.license = license
        self.mpg = mpg
    
    def getWheels(self):
        return 4

    def __repr__(self):
        return "Car(%r,%r)" % (self.license, self.mpg)


class Bike(Vehicle):
    def __init__(self,x=1):
       self.x = x 
    
    def getWheels(self):
        return 2     

    def __repr__(self):
        return "Bike()"








