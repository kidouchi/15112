# Kelly Idouchi + kidouchi + 1

# Place your solutions here!

def sumOfSquaresOfDigits(n):
    if( (type(n) != type(1)) or (n < 0)):
        return 0
    sum = 0
    while(n > 0):
        x = (n % 10)
        sum = sum + x**2
        n = n/10
    return sum

def isHappyNumber(n):
    if((n <= 0) or (type(n) != type(1))):
        return False
    while(n!= 1 and n!= 4):
        n = sumOfSquaresOfDigits(n)
    return(n == 1) and (n > 0)

def nthHappyNumber(n):
    i = 1
    happyCount = 0
    if ((n < 0) or (type(n) != type(1))):
        return 0
    while(happyCount < n):
        i += 1
        if isHappyNumber(i):
            happyCount += 1
    return i

def isPrime(n):
    if((n < 2) or (type(n) != type(1))):
        return False
    for factor in xrange(2,n):
        if(n % factor == 0):
            return False
    return True

def nthHappyPrime(n):
    i = 6
    happyCount = 0 
    if ((n < 0) or (type(n) != type(1))):
        return 0
    while(happyCount < n + 1):
        i += 1
        if(isHappyNumber(i) and isPrime(i)):
            happyCount += 1
    return i
                
def pi(n):
    i = 1
    count = 0
    if((n < 0) or (type(n) != type(1))):
        return 0
    while(i < n and n > 0):
        i += 1
        for factor in xrange(i, i + 1):
            if(isPrime(i)):
                count += 1        
    return count 

def h(n):
    i = 0
    sum = 0
    if((n <= 0) or (type(n) != type(1))):
        return 0
    while(n > 0 and i < n):
        i += 1
        for factor in xrange(i, i+1):
            sum = sum + 1.0/factor
    return sum        

def estimatedPi(n):
    if((n <= 2) or (type(n) != type(1))):
        return 0
    if(n > 2):
        return int(round((n/((h(n)) - 1.5))))

def estimatedPiError(n):
    if(n > 2):
        return abs(estimatedPi(n)-pi(n))
    if((n <= 2) or (type(n) != type(1))):
        return 0


######################################################################
##### ignore_rest: The autograder will ignore all code below here ####
######################################################################

def main():
    # include following line to autograde when you run this file
        execfile("lab2-public-grader.py", globals())

        if __name__ == "__main__":
            main()

#From 15-112 lessons - "almost equal" comparison doubles
#Python will take decimals and will not read it as 1.0 but maybe something like
#1.00000000000001 and that is not equal 1.0. This method helps create a certain 
# unit where it will stop the unending decimals. I'm using it in this case,
# because the function h(n) will definitely have recurring decimals and I want
# to be able to compare it to simpler decimals like 2.0 rather than 2.000001

def almostEquals(a, b):
    epsilon = 0.0000001
    return (abs(b-a) < epsilon)

def testing():
    assert(sumOfSquaresOfDigits(-1) == 0)
    assert(sumOfSquaresOfDigits(-19) == 0)
    assert(sumOfSquaresOfDigits(-123) == 0)
    assert(sumOfSquaresOfDigits(5) == 25)
    assert(sumOfSquaresOfDigits(12) == 5)
    assert(sumOfSquaresOfDigits(234) == 29)
    assert(sumOfSquaresOfDigits(1000) == 1)
    assert(sumOfSquaresOfDigits(1.0) == 0)
    assert(sumOfSquaresOfDigits(0.0) == 0)
    assert(sumOfSquaresOfDigits(3.557) == 0)
    assert(sumOfSquaresOfDigits(-5.23) == 0)
    assert(sumOfSquaresOfDigits("This sucks!") == 0)
    print "Passed 1!"

    assert(isHappyNumber(19) == True)
    assert(isHappyNumber(-7) == False)
    assert(isHappyNumber(1) == True)
    assert(isHappyNumber(2) == False)
    assert(isHappyNumber(97) == True)
    assert(isHappyNumber(98) == False)
    assert(isHappyNumber(404) == True)
    assert(isHappyNumber(405) == False)
    assert(isHappyNumber(496) == True)
    assert(isHappyNumber(1.0) == False)
    assert(isHappyNumber(0.0) == False)
    assert(isHappyNumber(3.557) == False)
    assert(isHappyNumber("This sucks!") == False)
    print "Passed 2!"

    assert(nthHappyNumber(0) == 1)
    assert(nthHappyNumber(1) == 7)
    assert(nthHappyNumber(2) == 10)
    assert(nthHappyNumber(3) == 13)
    assert(nthHappyNumber(4) == 19)
    assert(nthHappyNumber(5) == 23)
    assert(nthHappyNumber(6) == 28)
    assert(nthHappyNumber(7) == 31)
    assert(nthHappyNumber(-1) == 0)
    assert(nthHappyNumber(-123) == 0)
    assert(nthHappyNumber(0.0) == 0)
    assert(nthHappyNumber(1.0) == 0)
    assert(nthHappyNumber(-1.0) == 0)
    assert(nthHappyNumber(-3.578) == 0)
    assert(nthHappyNumber(3.577) == 0)
    assert(nthHappyNumber("This sucks!") == 0)
    print "Passed 3!"

    assert(nthHappyPrime(0) == 7)
    assert(nthHappyPrime(1) == 13)
    assert(nthHappyPrime(2) == 19)
    assert(nthHappyPrime(3) == 23)
    assert(nthHappyPrime(4) == 31)
    assert(nthHappyPrime(5) == 79)
    assert(nthHappyPrime(6) == 97)
    assert(nthHappyPrime(7) == 103)
    assert(nthHappyPrime(8) == 109)
    assert(nthHappyPrime(-5) == 0)
    assert(nthHappyPrime(-1) == 0)
    assert(nthHappyPrime(-123) == 0)
    assert(nthHappyPrime(0.0) == 0)
    assert(nthHappyPrime(1.0) == 0)
    assert(nthHappyPrime(3.577) == 0)
    assert(nthHappyPrime(-1.0) == 0)
    assert(nthHappyPrime(-3.567) == 0)
    assert(nthHappyPrime("This sucks!") == 0)
    print "Passed 4!"

    assert(pi(1) == 0)
    assert(pi(2) == 1)
    assert(pi(3) == 2)
    assert(pi(4) == 2)
    assert(pi(5) == 3)
    assert(pi(100) == 25)
    assert(pi(-123) == 0)
    assert(pi(0) == 0)
    assert(pi(0.0) == 0)
    assert(pi(-1) == 0)
    assert(pi(-1.0) == 0)
    assert(pi(-3.567) == 0)
    assert(pi(3.567) == 0)
    assert(pi("This sucks!") == 0)
    print "Passed 5!"

    assert(almostEquals(h(0), 0.0))
    assert(almostEquals(h(1), 1/1.0))
    assert(almostEquals(h(2), 1/1.0 + 1/2.0))
    assert(almostEquals(h(3), 1/1.0 + 1/2.0 + 1/3.0))
    assert(almostEquals(h("This sucks"), 0))
    assert(almostEquals(h(-1), 0))
    assert(almostEquals(h(-1.0), 0))
    assert(almostEquals(h(-3.577), 0))
    assert(almostEquals(h(3.568), 0))
    assert(almostEquals(h(-123), 0))
    print "Passed 6!"
    
    
    assert(estimatedPi(100) == 27)
    assert(estimatedPi(-1) == 0)
    assert(estimatedPi(-100) == 0)
    assert(estimatedPi(-123) == 0)
    assert(estimatedPi(0.0) == 0)
    assert(estimatedPi(-1.0) == 0)
    assert(estimatedPi(3.5567) == 0)
    assert(estimatedPi(-3.5677) == 0)
    assert(estimatedPi(1.0) == 0)
    assert(estimatedPi("This sucks!") == 0)
    print "Passed 7!"
    
        
    assert(estimatedPiError(100) == 2)
    assert(estimatedPiError(200) == 0)
    assert(estimatedPiError(300) == 1)
    assert(estimatedPiError(400) == 1)
    assert(estimatedPiError(500) == 1)
    assert(estimatedPiError(-100) == 0)
    assert(estimatedPiError(-1) == 0)
    assert(estimatedPiError(-123) == 0)
    assert(estimatedPiError(-1.0) == 0)
    assert(estimatedPiError(0.0) == 0)
    assert(estimatedPiError(1.0) == 0)
    assert(estimatedPiError(3.5657) == 0)
    assert(estimatedPiError(-1.0) == 0)
    assert(estimatedPiError(-3.5675) == 0)
    assert(estimatedPiError("This sucks!") == 0)
    print "Passed 8!"















