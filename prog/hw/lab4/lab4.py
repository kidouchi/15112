# Kelly Idouchi + kidouchi + Section B
#Group : Jacklin Wu -- jacklinw
#        Jolyn Sandford -- jsandfor
#        Stephanie Yang -- syyang
#Take string and finds the letter by n shifts and replaces with new letters

import string

#Encodes text by shifting each character of string by n shifts
def encodeText(s,n):
    result = ""
    lower = string.ascii_lowercase
    upper = string.ascii_uppercase
    if (len(s) == 0):
        return 0
    #Goes through each character of string
    for i in range(len(s)):  
        if(s[i].isalpha() == False):
            result += s[i]
        for x in range(len(lower)):
            #Shifts forward n places
            if(s[i] == lower[x]):
                #Makes sure if goes past "z" starts back at "a"
                result += lower[(x+n) % 26]
        for y in range(len(upper)):
            if(s[i] == upper[y]):
                result += upper[(y+n) % 26]
    return result

#Decodes text by shifting characters back n shifts
def decodeText(s,n):
    result = ""
    lower = string.ascii_lowercase
    upper = string.ascii_uppercase
    punct = string.punctuation
    if(len(s) == 0):
        return 0
    #Goes through each character
    for i in range(len(s)):
        if(s[i] == " "):
            result += " "
        for x in range(len(lower)):
            if(s[i] == lower[x]):
                #Shifts backwards n places
                result += lower[(26 +(x-n))%26]
        for y in range(len(upper)):
            if(s[i] == upper[y]):
                result += upper[(26+(y-n))%26] 
        for p in range(len(punct)):
            if(s[i] == punct[p]):
                result += punct[p]
    return result

def observed(s, clower, cupper):
    count = 0
    total = 0
    if(len(s) == 0):
        return 0
    #Counts how many times certain letter appears  
    for i in range(len(s)):
        if(s[i].isalpha()):
            total += 1
            if((s[i] == clower) or (s[i] == cupper)):
                count += 1
        #finds total alphabetical characters
    return float(count)/total

#Takes a string's chi squared value
def chiSquared(s):
    if(len(s) == 0):
        return 0
    chiE = (((observed(s, "e", "E") - 0.12702)**2) / 0.12702) 
    chiL = (((observed(s, "l", "L") - 0.04025)**2) / 0.04025)
    chiK = (((observed(s, "k", "K") - 0.00772)**2) / 0.00772)
    chi = chiE + chiL + chiK
    return chi

#Find shift with lowest chi squared value
def crackCode(s):  
    bestShift = 0
    if(len(s) == 0):
        return 0
    smallestSoFar = chiSquared(s)
    #Goes through all possible shifts
    for shift in range(len(string.ascii_lowercase)):
        chiNew = chiSquared(encodeText(s, shift))
        #Compares and finds smallest chi squared value
        if(chiNew < smallestSoFar):
            smallestSoFar = chiNew
            bestShift = 26-shift
    if(shift == 0):
        return 0
    return bestShift

#deletes comments
def deleteComments(code):
    start = 0
    end = 0
    if(code.find("#") == -1):
        return code
    while(code.find("#") != -1):
        start = code.find("#") 
        if(code.find("\n") != -1):
            end = code.find("\n")
        else:
            end = len(code) 
        code = code[:start] + code[end+1:]
    return code

#deletes anything between quotes
def deleteQuotes(c,code):
    start = 0
    end = 0
    if(code.find(c) == -1):
        return code
    while(code.find(c) != -1):
        start = code.find(c)
        if(code.find(c, start + 1) != -1):
            end = code.find(c, start +1)
        code = code[:start] + code[end+1:]
    return code 

#returns new code string
def newCode(code):
    code = code.replace("\\\'", "")
    code = code.replace('\\\"', "")
    code = deleteQuotes("'", code)
    code = deleteQuotes('"', code)
    code = deleteQuotes('"""', code)
    code = deleteComments(code)
    return code

#Checks to see if there is balanced amt of ()
def hasBalancedParentheses(code):
    code = newCode(code)
    totalParen = 0
    for c in code:
        if(c == "("):
            totalParen += 1
        elif(c == ")"):
            totalParen -= 1
        if(totalParen < 0):
            return False
    if(totalParen == 0):
        return True
    else:
        return False

#adds spaces evenly
def adjustLine(line, width):
    line = line.strip()
    if(len(line) > width):
        return (line[:width]strip() + "\n", line[width:])   
    leftoverSpace = width - len(line)   
    i = 0
    while(leftoverSpace > 0):
        i = line.find(" ", i)
        line = line[:i] + " " + line[i:]
        i += 2
     for word in line.split():
        if(
        leftoverSpace -= 1
        if((i > len(line)) or (line.find(" ", i) == -1)):
            i = 0
    line += "\n"
    return line, ""

#makes passage as long as given width
def justifyText(passage, width):
    newPassage = """\
"""
    line = ""
    length = 0
    for word in passage.split():
        if(len(word) == width):
            newPassage += word + "\n"
        if(len(word) > width):
            (newLine, line) = adjustLine(line, width)
            newPassage += newLine
            while(len(line) >= width):
                (newLine, line) = adjustLine(line, width)
                newPassage += newLine
        if((length + len(word)) > width):
            (newLine, line) = adjustLine(line, width)
            newPassage += newLine
        line = line + " " + word
        length = len(line)
    newPassage = newPassage + line[1:]
    return newPassage



######################################################################
##### ignore_rest: The autograder will ignore all code below here ####
######################################################################

#From Data and Expressions lesson
#Makes sure number doesn't have leading decimals
def almostEqual(d1, d2):
    epsilon = 0.000001
    return (abs(d2 - d1) < epsilon)

def testEncodeText():
    assert(encodeText("We attack at dawn!", 5) == "Bj fyyfhp fy ifbs!")
    assert(encodeText("Hello!", 3) == "Khoor!")
    assert(encodeText("""Hello!
                         Hello!
                         Hello!
                         """, 3) == "Khoor! Khoor! Khoor!")
    assert(encodeText("", 1) == 0)
    print "Passed!"


def testDecodeText():
    assert(decodeText("Bj fyyfhp fy ifbs!", 5) == "We attack at dawn!")
    assert(decodeText("Khoor!", 3) == "Hello!")
    assert(decodeText("", 1) == 0)
    print "Passed!"

def testObserved():
    assert(almostEqual(observed("Hello", "e", "E"), 0.2))
    assert(almostEqual(observed("Hello!?", "e", "E"), 0.2))
    assert(almostEqual(observed("", "l", "L"), 0.0))
    assert(almostEqual(observed("""Heck
                                   Heck
                                   Heck
                                   """, "k", "K"), 0.25))
    print "Passed!"


def testChiSquared():
    assert(almostEqual(chiSquared("eat kind lit!?"), 1.197501))
    assert(almostEqual(chiSquared("hello"), 3.265056))
    assert(almostEqual(chiSquared(""), 0.0))
    print "Passed!"

def testCrackCode():
    print "testing crackCode()...",
    plaintext = (
"""
CHAPTER I. Down the Rabbit-Hole
Alice was beginning to get very tired of sitting by her sister on the bank,
and of having nothing to do: once or twice she had peeped into the book her
sister was reading, but it had no pictures or conversations in it, 'and what
is the use of a book,' thought Alice 'without pictures or conversation?'
So she was considering in her own mind (as well as she could, for the hot day
made her feel very sleepy and stupid), whether the pleasure of making a
daisy-chain would be worth the trouble of getting up and picking the daisies,
when suddenly a White Rabbit with pink eyes ran close by her.
""")
    for shift in xrange(26):
        ciphertext = encodeText(plaintext, shift)
        crackedShift = crackCode(ciphertext)
        assert(shift == crackedShift)
    print "passed!"

def testHasBalancedParentheses():
    assert(hasBalancedParentheses("(((") == False)
    assert(hasBalancedParentheses("()()()") == True)
    assert(hasBalancedParentheses("(are) '((you(' \'(the(\' tooth faerie \\\" () \\\" ()?\")\"") == True)
    assert(hasBalancedParentheses(")(") == False)
    print "Done!"

#tests the function workableCode
def testNewCode():
    print "testing workableCode..."
    assert(newCode('sdsd\\"dsfdsfd')=='sdsddsfdsfd')
    assert(newCode("sdsd\\'dsfdsfd")=="sdsddsfdsfd")
    assert(newCode('hello\"\"\"\nTHIS IS GUNNA BE DELEEEETED\n\"\"\" BROTHAH')=='hello BROTHAH')
    assert(newCode('she said, \'she shells\'')=="she said, ")
    assert(newCode('she said, \"she said!!\"')=="she said, ")
    assert(newCode('omg, like, what the #insert curse word')=="omg, like, what the ")
    print "Passed!" 

def testJustifyText():
    assert(justifyText("""\We hold these truths to be self-evident:  that all men are created equal;that they are endowed by their Creator with certain unalienable rights;that among these are life, liberty, and the pursuit of happiness.""", 30)== """\We  hold  these  truths  to be
    self-evident: that all men are
    created  equal;  that they are
    endowed  by their Creator with
    certain   unalienable  rights;
    that  among  these  are  life,
    liberty,  and  the  pursuit of
    happiness.""")



