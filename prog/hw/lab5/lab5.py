# lab5.py
# Members : Kelly Idouchi(kidouchi), Jolyn Sandford(jsandfor), Jackie Wu(jacklinw), Stephanie Yang(syyang), Andrew Zadravec(azadrave) 
# Date of Meet-up : October 16, 2011, October 17, 2011
# Time : 10/16 --> 4:30 - 6:30pm    10/17 --> 5:30 - 7:30pm
# Place : 10/16 --> Gates 6th Floor   10/17 --> Donner Reading Room

"""
Week #1 Problems 
1. Find how many nxn squares can fit in a x by y rectangle?
Solution:
def numberOfSquares(x, y, n):
    x = int(x)
    y = int(y)
    n = int(n)
    length = y/n
    width = x/n
    area = length * width
    return area

2. Write a function that takes an integer and finds the nth digit.
Solution:
def findDigit(n, digit):
    return int((n/(10**digit))%10)

3. Write a function that takes a color in hex and returns each part of the color
Solution:
def decodeRGBA(n):
    red = n << 24
    blue = (n & 0x00ff0000) << 16
    green = (n & 0x0000ff00) << 8
    alpha = (n & 0x000000ff)


4. Print the output:
def f(x):
    a = 0b11111
    b = 0
    c = False
    d = True
    e = sdlkfjdsaf
    f = 42
    g = 80
    h = 33
     i)    print (a|h)
    ii)    print (g^h) 
   iii)    print (f&g)
    iv)    print (d and c)
     v)    print (c or d)
    vi)    print (e or d)
   vii)    print (a or e)
  viii)    print (f or g)
    ix)    print (f and g)
    
Solutions:
     i) 127
    ii) 113
   iii) 0
    iv) False
     v) True
    vi) sdlkfjdsaf
   vii) 31
  viii) 42
    ix) 80

---------------------------------------------------
Week #2 Problems 
1.) Correct the following the problems :
        a.)print "Hello"
           print "world!"
           Wanted output : Hello world! 
            Solution : print "Hello",
                       print "world!"
        
        b.)x = raw_input("Enter a number:  ")
           print  "One fifth of", x, "=", x/5
            Solution : x = int(raw_input("Enter a number: ")
        
        c.) print math.sqrt(5)
            Solution : import math

        d.) print "x =", x
            Solution : must declare x

        e.) Explain difference between input and raw_input.
            Solution : input does not catch user errors

        f.) What will this print :
            x = 3
            x = 5
            print x
            Solution : 5

2.) Answer the following questions :
    a.)
        if(x < 3):
            print "Yes!"
        elif(x == 5):
            print "Hah!"
        else:
            print "Awww!"

    What will the following print :
        i.) x = 5
        Solution : 'Hah!'
        ii.) x = 10
        Solution : 'Awww!'
        iii.) x = 1
        Solution : 'Yes!'

    b.) What will the following print :

        # A rating from 1 - 10
        rating = 7
        if(num >= 6 and num <= 10):
            print "Top rating!"
        elif(num >= 1 and num <= 5):
            print "Low rating..."
        Solution : 'Top rating!'
    
    c.) What will the following print:

        i.) print not True or False
        Solution : False
        ii.) print not(True or False)
        Solution : False

3.) Is your number abundant enough? 
Create a function that finds the nth abundant number. An abundant number is a number for which the sum of its factors or divisors is greater than the number itself.
For example:
24 is an abundant number because its divisors, which are 1,2,3,4,6,8,12, sum up to 36 and is greater than 24. Therefore 24 is an abundant number.

Solution:
def sumOfDivisors(num):
    sum = 1
    for test in xrange(2, num/2 + 1):
        if(num % test == 0):
            sum += test
    return sum

def nthAbundantNumber(n):
    count = 0
    i = 1
    while(count < n):
        i += 1
        if(sumOfDivisors(i) > i):
            count += 1
    return i

4.) Can math define the meaning of life?
Create a function that finds the nth life number (This number was made up by the group and therefore does not really exist). A life number is defined by the number 42 (Credited from "Hitchhiker's Guide to the Galaxy"). If the number contains at least one 42 within itself, then it is considered a life number.
For example:
342  is a life number because it contains 42.
402  is not a life number because, although it contains 4 and 2, they are not adjacent to each other. We are specifically looking for numbers with 42 within itself. 
342542 is a life number, because this number contains at least one 42 within itself.

Solution:
def isLife(num):
    lifeOnes = 2
    lifeTens = 4
    while(num > 0):
        digit = num % 10
        if(digit == lifeOnes):
            if(findDigit(num, 1) == lifeTens):
                return True
        num /= 10
    return False

def nthLifeNumber(n):
    count = 0
    i = 0
    while(count < n):
        i += 1
        if(isLife(i)):
            count += 1
    return i

------------------------------------------
Week #3 Problems 
1. Put the following common function families in order by slowest to fastest:

                       logk(n), kn, sqrt(n), k**n, k(n)**2, nlogn, k
Solution : k**n, k(n)**2, nlogn, kn, sqrt(n), logk(n), k 

2. Recall the solveSudoku function. Write the main function using helper functions, and underneath your main code, write what each of the helper functions should do.
Solution:
def solveSudoku(puzzle):
    newPuzzle = []
    oldPuzzle = puzzle
  maxRow = len(puzzle)
    maxCol = maxRow
    list = [0] * maxCol
    while (oldPuzzle != newPuzzle):
        oldPuzzle = newPuzzle
        list = [0] * maxCol
        row = puzzle.find(0)
        if row != -1:
            col = puzzle[row].find(0)
            checkRow(oldPuzzle, list, col) #goes through row, looks for 0s
            checkCol(oldPuzzle, list, row) #goes through col, looks for 0s
            checkBlock(oldPuzzle, list, row, col) # "" block, ""
            if list.count(0) == 1:
                i = list.find(0)
                oldPuzzle[row][col] = i+1
                newPuzzle = oldPuzzle
    return newPuzzle

3. Find the Big O notation of the following for this function:

    def countOdds(a):
    count = 0
    for item in a:
        if (item % 2 == 1):
            count += 1
    return count

Runtime: O(n)

Count: O(n)

4. Find the Big O Notation of the following for this function:

        def doop(n):
            i = 0
            result1 = 0
            result2 = 1
                while (i<n):
                    for j in xrange(n):
                        result1 += 5
                    for k in xrange(n):
                        result2 *= 5
                    i += 1
            totalResult = result1 + result2
            return totalResult

Runtime: O(n**2)

result1: O(n**2)

result2: O(5**(n**2))

totalResult: O(5**(n**2))
-------------------------------------
Week #4 Problems
1. What will this function print?

def mysteryFunction():
    s = "a  b  c  d  e  f  g  h  i  j  k   l   m   n   o    p "
         0  1  2  3  4  5  6  7  8  9  10  11  12  13  14   15
    a = s[:4]
    b = s[7:]
    c = s[3:10]
    d = s[4:]
    e = s[10:100]
    f = s[-3]
    g = len(s)
      i)    print c
     ii)    print (a+c)
    iii)    print (b+d)
     iv)    print e
      v)    print f
     vi)    print g
    vii)    print (a+d)

solution
      i)    defghij
     ii)    abcddefghij
    iii)    hijklmnopefghijklmnop
     iv)    klmnop
      v)    n
     vi)    16
    vii)    abcdefghijklmnop
   
2. Improve this code for style, clarity, and efficiency

def thisFunctionDoesSomething(foo):
    count = 0
    for number in range(foo):
        for number2 in range(foo):
            for number3 in range(foo):
                count += 1
    return count
Solution : 
def cubed(num):
    return num**3 

3.
code/cipher - output letters as numbers, separated by dashes (normal punctuation)
e.g., "stop" --> 19-20-15-16
For this problem, a = 1, b = 2, c = 3, d = 4, etc.
assume no numbers in message? or we can put in a special signifier for actual numbers, e.g., *19 for the actual number 19
Solution:
import string
def codeIntoNumbers(message):
    message = message.upper()
    newMessage = ""
    cDiff = 64 
    for i in xrange(len(message)):
        if(message[i].isalpha()):
            newMessage += string(ord(message[i]) - cDiff) 
            if(i < len(message) -1):
                newMessage += "-"
    return newMessage
    
4.
Mad libs (using %s, %d, etc. formatting) - prompt user for words, numbers, etc. and output them correctly
Solution:
def madLibs():
    x = raw_input("Name = ")
    y = raw_input("Number = ")
    s = "I went to %s and had %d milkshakes!" % (x,y)
    print s

---------------------------------------
Week #5 Problems
1.  a. In the following code, identify any preconditions and postconditions and why they are present in the code.
        def square(x):
            **Solution : precondition** 
            assert (isinstance(x, int) or isinstance(x, float) or isinstance(x, long))
            xSquared=x**2
            **Solution : postcondition**
             assert (isinstance(xSquared, int) or isinstance(xSquared, float) or isinstance(xSquared, long))
            return xSquared
    
    b. Why would invariants not be used in this function?
            Solution : Invariants are more relevant with any type of loop
    
    c. Assuming the gray code is given, rewrite the function implementing contracts.
            Solution :
            def square_requires(x):
               assert(isinstance(x, int) or isinstance(x, float), or isinstance(x,long))
            def square_ensures(result, x):
                assert(isinstance(result, int) or isinstance(result, float) or isinstance(result, long)) 

2. Improve this function so that it works as intended for any input:
   Solution:
    def add5():
        number = raw_input("Enter an integer: ")
        if(not isinstance(number, int)):
            return False
        return int(number)+5

3. What is printed by:
    a=[1, 2, 3, 4]
    b=a
    b[0]=0
    print a, b
    Solution : [0,2,3,4] [0,2,3,4]

4. What is  printed by:
    a=[5, 6, 7]
    b=[]
    for element in a:
        b = b + [element/2]
    print a, b

    Solution : [5,6,7] [2,3,3]
-------------------------------------------
Week #6 Problems
1. Print out these numbers in a 2D list:
     a) every number
     b) every number backwards, e.g opposite of a
     c) a given sudoku row
     d) a given sudoku column
     e) a sudoku square

Solution : 
a) 
def everyNum(list):
    row = len(list)
    col = len(list[0])
    for rows in row:
        for cols in col:
            print list[rows][cols]
b.) 
def numBackwards(list):
    row = len(list)
    col = len(list[0])
    for rRows in reversed(row):
        for rCols in reversed(col):
            print list[rRows][rCols]
c.) 
def sudokuRow(puzzle, row):
        print puzzle[row]
d.) 
def sudokuCol(puzzle, col):
    row = len(puzzle)
    for rows in row:
        print puzzle[row][col]

e.)
def sudokuCol(puzzle, block):
    row = len(puzzle)
    col = len(puzzle[0])
    n = int(row**(1.0/2))
    startRow =  n(block / n)
    startCol =  n(block % n)
    endRow = startRow + n
    endCol = startCol + n
    for rows in (startRow, endRow):
        for cols in (startCol, endCol):
            print puzzle[rows][cols]


2. Write a function that manually (not using the function max) finds the largest number in each column and adds them up
Solution:
def largestNumInCol(list):
    row = len(list)
    col = len(list[0])
    sum = 0
    for cols in col:
        largestSoFar = 0
        for rows in row:
            if(list[rows][cols] > largestSoFar):
                largestSoFar = list[rows][cols]
        sum += largestSoFar
    return sum
        
3. Write a function that adds the diagonal (from top left to bottom right) in a 2D list, assuming that the list is N X N
Solution:
def diagonalSum(list):
    row = len(list)
    sum = 0
    for x in xrange(row):
        sum += list[x][x]
    return sum

4. Recall the function that maps an integer to a rational number. Write the function.
Solution:
def intToRational(n):
    (x, y) = (0,0)
    for i in xrange(n):
        (x, y) = nextRational(x, y)
     return x, y

def nextRational(x, y):
    diagonal = x+y
    isEvenRow = (diagonal%2) == 0
    if isEvenRow:
        x = x+1
        if y != 0:
            y = y-1
    else:
        if x != 0:
            x = x-1
            y = y+1
    return (x, y)

--------------------------------------------
Week #7 Problems
1. Replicate a Concepts problem using sets (e.g., midterm - ( (S /\ T)c = Sc U Tc )

Solution:
u = set([1,2,3,4,5,6,7,8,9,10])
s = set([1,3,4,7,8])
t = set([5,6,7,8,9])
sC = u.difference(s)
tC = u.difference(t)
sUt = s.union(t)
sUt_C = u.difference(sUt)
sCUtC = sC.union(tC)
print "sUt_C == sCUtC: ", (sUt_C == sCUtC)


2.
s = set([2,2,4,6,6,2,5,7])
t = set([6,7,8,9,10])
u = set([7,8,9])
v = set(t.difference(u))
union = s.union(t)
intersection = s.intersection(t)

1) print (3 in s)               Solution : False
2) print (6 in s)               Solution : True
3) print (2 in union)           Solution : True
4) print (2 in intersection)    Solution : False 
5) print (6 in union)           Solution : True
6) print (6 in intersection)    Solution : True
8) print (u.issubset(t))        Solution : True
9) print (t.issuperset(u))      Solution : True
10) print v                     Solution : [6,10]

3. Create a function that, given a dictionary, finds the most common value and returns all of the corresponding keys. If there is no most common value return None.
Solution:
def mostCommonValue(d):
    common = {}
    max = 1
    for key in d:
        if(d[key] not in common):
            common[d[key]] = 1
        elif(d[key] in common):
            common[d[key]] += 1
    for value in common:
        if(common[value] > max):
            max = common[value]
    if(max == 1):
        return None
    else:
        return max    

4. Using a dictionary, count how many times the words "a", "the", "I" and "and" appear in a passage of text given as a string, and return those values.
Solution:
def countWords(passage):
    passage = (passage.upper()).split()
    countWord = {}
    countWord["A"] = 0
    countWord["THE"] = 0
    countWord["I"] = 0
    countWord["AND"] = 0
    for word in passage:
        if(word == "A"):
            countWord["A"] += 1
        if (word == "THE"):
            countWord["THE"] += 1
        if (word == "I"):
            countWord["I"] += 1
        if (word == "AND"):
            countWord["AND"] += 1
    return (countWord["A"], countWord["THE"], countWord["I"], countWord["AND"])
""" 







