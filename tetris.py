# tetris.py
# Group Members : Jacklin Wu (jacklinw)(C), Stephanie Yang (syyang)(C)
from Tkinter import *
import random
def mousePressed(event):
    redrawAll()

def keyPressed(event):  
    # for now, for testing purposes, just choose a new falling piece
    # whenever ANY key is pressed!
    if(event.keysym == "r"):
        init()
    elif(event.keysym == "Up"):
        rotateFallingPiece()
    elif(event.keysym == "Left"):
        moveFallingPiece(0, -1)
    elif(event.keysym == "Right"):
        moveFallingPiece(0, +1)
    elif(event.keysym == "Down"):
        moveFallingPiece(+1, 0)
    redrawAll()

#given list in a row
def isFullRow(row):
    emptyColor = canvas.data.emptyColor
    for index in xrange(len(row)):
        if(row[index] == emptyColor):
            return False
    return True

#takes list
def makeEmpty():
    emptyList = [ ]
    cols = canvas.data.cols
    emptyColor = canvas.data.emptyColor
    for index in xrange(cols):
        emptyList += [emptyColor]
    return emptyList 

def removeFullRows():
    board = canvas.data.board
    rows = canvas.data.rows
    cols = canvas.data.cols
    fullRows = 0 
    oldRow = [ ]
    #goes from bottom row to top 
    for row in xrange(rows-1,-1,-1):
        oldRow = [ ]
        for col in xrange(cols):
            oldRow += [board[row][col]]
        if(isFullRow(oldRow)):
            fullRows += 1
            board.pop(row)
    canvas.data.score += fullRows**2
    for addRow in xrange(fullRows):
        board.insert(addRow, makeEmpty())

#makes sure piece not off board
def fallingPieceIsLegal():
    fallingPieces = canvas.data.fallingPieces
    isEmptyColor = canvas.data.emptyColor
    rows = len(fallingPieces)
    cols = len(fallingPieces[0])
    top = canvas.data.fallingPieceRow
    left = canvas.data.fallingPieceCol
    board = canvas.data.board
    brows = len(board)
    bcols = len(board[0])
    for row in xrange(rows):
        for col in xrange(cols):
            if(fallingPieces[row][col] == True):
                if(top+row > brows-1 or left+col > bcols-1):
                    return False
                elif(top+row < 0 or left+col < 0):
                    return False
                elif(board[top+row][left+col] != isEmptyColor):
                    return False
    return True

def moveFallingPiece(drow, dcol):
    top = canvas.data.fallingPieceRow 
    left = canvas.data.fallingPieceCol 
    canvas.data.fallingPieceRow = canvas.data.fallingPieceRow + drow
    canvas.data.fallingPieceCol = canvas.data.fallingPieceCol + dcol
    #if legal piece, will move
    if(not fallingPieceIsLegal()): 
        canvas.data.fallingPieceRow = top
        canvas.data.fallingPieceCol = left
        return False
    return True

def fallingPieceCenter():
    piece = canvas.data.fallingPieces
    rows = len(piece)
    cols = len(piece[0])
    row = rows/2
    col = cols/2
    #find center of piece
    return (row,col)

def rotateFallingPiece():
    #original center 
    (oldCenterRow, oldCenterCol) = fallingPieceCenter() 
    #original location
    posX = canvas.data.fallingPieceRow
    posY = canvas.data.fallingPieceCol
    #original dimension
    rows = canvas.data.fallingPieceRows
    cols = canvas.data.fallingPieceCols
    #2d list of old piece
    piece = canvas.data.fallingPieces
    #rotating piece
    rotated = [ ]
    for col in xrange(len(piece[0])-1,-1,-1):
        newRow = [ ]
        for row in xrange(len(piece)):
            newRow += [piece[row][col]]
        rotated += [newRow]    
    canvas.data.fallingPieces = rotated
    #new center
    (newCenterRow, newCenterCol) = fallingPieceCenter()
    #position center 
    canvas.data.fallingPieceRow += oldCenterRow - newCenterRow
    canvas.data.fallingPieceCol += oldCenterCol - newCenterCol
    if(not fallingPieceIsLegal()):
        canvas.data.fallingPieces = piece
        canvas.data.fallingPieceRow = posX
        canvas.data.fallingPieceCol = posY
        canvas.data.fallingPieceRows = rows
        canvas.data.fallingPieceCols = cols

def timerFired():
    if(moveFallingPiece(+1,0) == False):
        placeFallingPiece()
        newFallingPiece()
        if(fallingPieceIsLegal() == False):
            canvas.data.isGameOver = True
    delay = 250 # milliseconds
    canvas.after(delay, timerFired) # pause, then call timerFired again
    redrawAll()

def drawScore():
    space = 15
    canvas.create_text(2*space,space,text = "SCORE:",font = "Helvetica 12" )
    canvas.create_text(5*space,space,text = canvas.data.score, font = "Helvetica 12")

def redrawAll():
    canvas.delete(ALL)
    if(canvas.data.isGameOver == False):
        drawGame()
        drawFallingPiece()
        drawScore()
    else:   
        gameOverScreen()

#makes new board
#empty with only the color blue
def loadBoard():
    cols = canvas.data.cols
    rows = canvas.data.rows
    canvas.data.board = [ (["blue"] * cols) for row in xrange(rows)]
    board = canvas.data.board

def drawGame():
    width = canvas.data.canvasWidth
    height = canvas.data.canvasHeight
    canvas.create_rectangle(0,0,width,height,fill = "orange")
    drawBoard()

def drawBoard():
    board = canvas.data.board
    rows = len(board)
    cols = len(board[0])
    for row in xrange(rows):
        for col in xrange(cols):       
            drawCell(board,row,col,board[row][col])

def drawFallingPiece():
    fallingPieces = canvas.data.fallingPieces
    fallingPieceColors = canvas.data.fallingPieceColors
    rowLen = len(fallingPieces)
    colLen = len(fallingPieces[0])
    fPRow = canvas.data.fallingPieceRow
    fPCol = canvas.data.fallingPieceCol
    for row in xrange(rowLen):
        for col in xrange(colLen):
            if(fallingPieces[row][col] == True):
               drawCell(fallingPieces,row+fPRow,col+fPCol,fallingPieceColors)

def placeFallingPiece():
    board = canvas.data.board
    fallingPieces = canvas.data.fallingPieces
    pieceColor = canvas.data.fallingPieceColors
    rowLen = len(fallingPieces)
    colLen = len(fallingPieces[0])
    frow = canvas.data.fallingPieceRow
    fcol = canvas.data.fallingPieceCol
    for row in xrange(rowLen):
        for col in xrange(colLen):
            if(fallingPieces[row][col] == True):
                board[row+frow][col+fcol] = pieceColor                 
    newFallingPiece()
    removeFullRows()

#taken from snake.py
def drawCell(fallingPiece,rows,cols,color):
    cellSize = 30
    board = canvas.data.board
    width = canvas.data.canvasWidth
    height = canvas.data.canvasHeight
    bw = len(board[0])*cellSize
    bh = len(board)*cellSize
    marginW = (width - bw)/2
    marginH = (height-bh)/2
    left = marginW + cols* cellSize
    right = left + cellSize
    top = marginH + rows * cellSize
    bottom = top + cellSize 
    canvas.create_rectangle(left,top,right,bottom, fill = "black")
    canvas.create_rectangle(left+2,top+2,right-2,bottom-2, fill = color)
  
def tetrisPieces():
    sPiece = [
    [False, True, True],
    [True, True, False]
    ]
    iPiece = [
    [True, True, True, True]
    ]
    jPiece = [
    [True, False, False],
    [True, True, True]
    ]
    lPiece = [
    [False, False, True],
    [True, True, True]
    ]
    oPiece = [
    [True, True],
    [True, True]
    ]
    tPiece = [
    [False, True, False],
    [True, True, True]
    ]
    zPiece = [
    [True, True, False],
    [False, True, True]
    ]
    tetrisPieces = [iPiece, jPiece, lPiece, oPiece, sPiece, 
    tPiece, zPiece]
    return tetrisPieces

def tetrisPieceColors():    
    tetrisPieceColors = ["red", "yellow", "magenta", "pink",
    "cyan", "green", "orange"]
    return tetrisPieceColors

def gameOverScreen():
    board = canvas.data.board
    width = canvas.data.canvasWidth
    height = canvas.data.canvasHeight
    canvas.create_rectangle(0,0,width,height, fill = "white")
    canvas.create_text(width/2,height/2,text ="GAME OVER!!!",font=("Helvetica",32))
    canvas.create_text(width/2,height/2 + 30, text = "Wanna restart? Press r", font =("Helvetica",12))
    
def newFallingPiece():
    board = canvas.data.board
    tetrisPieces = canvas.data.tetrisPieces
    tetrisPieceColors = canvas.data.tetrisPieceColors 
    boardWidth = canvas.data.cols 
    index = random.randint(0,6)
    #find fallingPiece and its color
    canvas.data.fallingPieces = tetrisPieces[index]
    fallingPieces = canvas.data.fallingPieces
    canvas.data.fallingPieceColors = tetrisPieceColors[index]
    #find center for fallingPiece
    canvas.data.fallingPieceRow = 0
    canvas.data.fallingPieceCol = boardWidth/2 - len(fallingPieces[0])/2
    canvas.data.fallingPieceRows = len(fallingPieces)  
    canvas.data.fallingPieceCols = len(fallingPieces[0])

def init():
    loadBoard()
    canvas.data.tetrisPieces = tetrisPieces()
    canvas.data.tetrisPieceColors = tetrisPieceColors() 
    canvas.data.emptyColor = "blue"
    canvas.data.isGameOver = False
    canvas.data.score = 0
    newFallingPiece()
    redrawAll()
    
########### copy-paste below here ###########

def run(rows,cols):
    # create the root and the canvas
    global canvas
    root = Tk()
    canvasWidth = 350
    canvasHeight = 500
    canvas = Canvas(root, width=canvasWidth, height=canvasHeight)
    canvas.pack()
    root.resizable(width = 0, height = 0)
    # Store canvas in root and in canvas itself for callbacks
    root.canvas = canvas.canvas = canvas
    # Set up canvas data and call init
    class Struct: pass
    canvas.data = Struct()
    canvas.data.rows = rows
    canvas.data.cols = cols
    canvas.data.canvasWidth = canvasWidth
    canvas.data.canvasHeight = canvasHeight
    init()
    # set up events
    root.bind("<Button-1>", mousePressed)
    root.bind("<Key>", keyPressed)
    timerFired()
    # and launch the app
    root.mainloop()  # This call BLOCKS (so your program waits until you close the window!)

run(15,10)

